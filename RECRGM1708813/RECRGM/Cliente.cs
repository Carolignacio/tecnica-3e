﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RECRGM
{
    class Cliente: Pessoa
    {
        private double limiteCredito;
        private int cartaoCredito;
        private string contato;
        private bool status;

        public double LimiteCredito { get => limiteCredito; set => limiteCredito = value; }
        public int CartaoCredito { get => cartaoCredito; set => cartaoCredito = value; }
        public string Contato { get => contato; set => contato = value; }
        public bool Status { get => status; set => status = value; }
        public double verificaCredito()
        {
            return 0;  
        }

        public int validaCartao()
        {
            return 0;
        }
        public Cliente()
        { }
    }

}
